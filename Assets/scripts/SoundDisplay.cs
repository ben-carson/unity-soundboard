﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundDisplay : MonoBehaviour {

	private int audioSize = 5;
	private bool isMusicPlaying = false;
	public Text counterText;
	public AudioSource errorSound;

	// Update is called once per frame
	void Update () {
		counterText.text = "Counter: " + audioSize;
		if(Input.GetKeyDown(KeyCode.Space) && audioSize > 0) {
			audioSize--;
		} else if (Input.GetKeyDown(KeyCode.Space) && audioSize <= 0) {
			errorSound.Play();
		}

		if(Input.GetKeyDown(KeyCode.M) && isMusicPlaying)
		{
			isMusicPlaying = false;
			//TODO: stop playing music 
		} else {
			isMusicPlaying = true;
			//TODO: start playing music
		}
	}
}
