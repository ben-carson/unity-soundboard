using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFile : ScriptableObject
{
    public string fileName;
    public string filePath;
    public int indexValue;

}