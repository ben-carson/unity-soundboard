﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHandler : MonoBehaviour {

	public AudioSource openAppSoundSource;
	public AudioSource Sound1Source;
	public AudioSource Sound2Source;
	public AudioSource closeAppSoundSource;

	public string rootFilePath;
	private bool closeAppFlag;
	private bool playBGM;

	public void Start() 
	{
		closeAppFlag = false;
		playBGM = false;
		openAppSoundSource.Play();
		FileSearcher fTest = new FileSearcher();
		fTest.GetAudioFiles(rootFilePath, fTest.AudioFileType); //the default file type is 'wav', which is what we want.
	}

	public void Update()
	{
		if(closeAppFlag && !closeAppSoundSource.isPlaying)
		{
			Application.Quit();
		}
	}

	public void DisplayTestText() 
	{
		Debug.Log("Hello, World!");
		Sound1Source.Play();
	}

	public void PlaySound2()
	{
		Sound2Source.Play();
	}

	public void exitApp() 
	{
		closeAppFlag = true;
		closeAppSoundSource.Play();
	}
}
